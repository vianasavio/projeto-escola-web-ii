class Quetao {

    constructor() {
        this._tipoQuestaoEl = '';
        this._respostaEl = '';
        this._respostaConteudoEl = '';
        this._respostaRodapeEl = '';
        this._btnAddAlternativaEl;
        this._contAlternativa = -1;
    }
    set tipoQuestaoEl(value) {
        this._contAlternativa = -1;
        this._tipoQuestaoEl = value;
        this._respostaEl = $(this._tipoQuestaoEl).parent().parent().find('.resposta');
        this._respostaEl.html('');
        this._respostaEl.html(`
            <div class="resposta-conteudo"></div>
            <div class="resposta-rodape"></div>
        `);

        this._respostaConteudoEl = $(this._respostaEl).find('.resposta-conteudo');
        this._respostaRodapeEl = $(this._respostaEl).find('.resposta-rodape');
    }

    addEvent(qtdAlternativa) {
        this._contAlternativa = qtdAlternativa - 1;
        this._tipoQuestaoEl = $('.tipo-questao');
        this._respostaEl = $(this._tipoQuestaoEl).parent().parent().find('.resposta');
        this._respostaConteudoEl = $(this._respostaEl).find('.resposta-conteudo');
        this._respostaRodapeEl = $(this._respostaEl).find('.resposta-rodape');
    }

    addAlternativa() {
        this._respostaConteudoEl.append(`
            <div class="form-group">
                <label for="TxAlternativa" class="control-label">Descrição da alternativa</label>
                <input name="Alternativa[${this._contAlternativa + 1}].TxAlternativa" id="TxAlternativa" class="form-control" />
                <button class="btn btn-info add-alternativa" type="button" >adicionar</button>
                <span asp-validation-for="TxAlternativa" class="text-danger"></span>
            </div>
        `);
    }

    addAlternativas() {
        this.addAlternativa();
        this._respostaRodapeEl.append(`
            <button class="btn btn-success mb-2 add-nova-alternativa" type="button"><i class="zmdi zmdi-plus"></i></button>
        `);
    }

    setAlternativaCorretaValue() {
        let alternativas = $('.alternativaCorretaValue');
        [...alternativas].forEach(element => {
            if ($(element).parent().find('.alternativaCorreta').is(':checked') == true) {
                $(element).val('S');
            } else {
                $(element).val('N');
            }
        });
    }

    saveAltervativa() {
        let txtAlternativa = $(this._btnAddAlternativaEl).parent().find('input').val();
        $(this._btnAddAlternativaEl).parent().remove();
        this._respostaConteudoEl.append(`
            <div class="custom-control custom-radio">
                <input type="radio" class="alternativaCorreta" name="alternativaCorreta" class="custom-control-input">
                <input type="hidden" class="alternativaCorretaValue" name="Alternativa[${this._contAlternativa + 1}].Flcorreta" value="">
                <input type="hidden" name="Alternativa[${this._contAlternativa + 1}].TxAlternativa" value="${txtAlternativa}">
                <label class="custom-control-label" for="customRadio1">${txtAlternativa}</label>
            </div>
        `);

        this._contAlternativa = this._contAlternativa + 1;
    }

    removeAlernativa() {

    }

    addResposta() {

        this._respostaConteudoEl.append(`
            <div class="form-group">
                <label for="Txresposta">Solução</label>
                <textarea class="form-control" id="Txresposta" name='Resposta[0].Txresposta' placeholder="Descreva a resposta desta Questão aqui!" rows="5"></textarea>
                <span asp-validation-for="Resposta[${this._contAlternativa + 1}].Txresposta" class="text-danger"></span>
            </div>
        `);
    }

    removeResposta() {

    }

    addOptions() {
        if ($(this._tipoQuestaoEl).val() == 1) {
            this.addAlternativas();
        } else if ($(this._tipoQuestaoEl).val() == 2) {
            this.addResposta();
        }
    }
}

var questao = new Quetao();

$(document).ready(function () {
    if (typeof (qtdAlternativa) != 'undefined') {
        questao.addEvent(qtdAlternativa);
    }
    $(document).on('change', '.tipo-questao', function () {
        questao.tipoQuestaoEl = this;
        questao.addOptions();
    });

    $(document).on('click', '.add-alternativa', function () {
        questao._btnAddAlternativaEl = this;
        questao.saveAltervativa();
    });

    $(document).on('click', '.add-nova-alternativa', function () {
        questao.addAlternativa();
    });

    $(document).on('click', '.alternativaCorreta', function () {
        questao.setAlternativaCorretaValue();
    });
});