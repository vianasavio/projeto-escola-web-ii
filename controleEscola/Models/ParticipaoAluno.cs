﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class ParticipaoAluno
    {
        public int ParticipacaoAlunoId { get; set; }
        public int AlunoAleatorioFk { get; set; }
        public DateTime Dtsorteio { get; set; }
        public short? Nrrodada { get; set; }
        public decimal VlnotaParticipacao { get; set; }

        public AlunoAleatorio AlunoAleatorioFkNavigation { get; set; }
    }
}
