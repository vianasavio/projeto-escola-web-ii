﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class Curso
    {
        public Curso()
        {
            Disciplina = new HashSet<Disciplina>();
        }

        public int CursoId { get; set; }
        public int? NivelCursoFk { get; set; }
        public string Nmcurso { get; set; }
        public string Dscurso { get; set; }

        public NivelCurso NivelCursoFkNavigation { get; set; }
        public ICollection<Disciplina> Disciplina { get; set; }
    }
}
