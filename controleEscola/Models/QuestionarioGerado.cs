﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class QuestionarioGerado
    {
        public int QuestionarioGeradoId { get; set; }
        public int QuestaoFk { get; set; }
        public int QuestionarioFk { get; set; }
        public DateTime Dtcriacao { get; set; }
        public DateTime? Dtatualizacao { get; set; }

        public Questao QuestaoFkNavigation { get; set; }
        public Questionario QuestionarioFkNavigation { get; set; }
    }
}
