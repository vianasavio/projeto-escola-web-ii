﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace controleEscola.Models
{
    public partial class Questionario
    {
        public Questionario()
        {
            QuestionarioGerado = new HashSet<QuestionarioGerado>();
        }

        public int QuestionarioId { get; set; }
        public int? UsuarioEscolaFk { get; set; }
        [Display(Name="Titulo")]
        public string Nmtitulo { get; set; }
        [Display(Name="Descricão")]
        public string Dsquestionario { get; set; }
        [Display(Name="Data Criação")]
        public byte[] Dtcriacao { get; set; }
        [Display(Name="Data Atualização")]
        public DateTime? Dtatualizacao { get; set; }
        public string FlAtivo { get; set; }

        public UsuarioEscola UsuarioEscolaFkNavigation { get; set; }
        public ICollection<QuestionarioGerado> QuestionarioGerado { get; set; }
    }
}
