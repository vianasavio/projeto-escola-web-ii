﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class Alternativa
    {
        public int AlternativaId { get; set; }
        public int QuestaoFk { get; set; }
        public string Txalternativa { get; set; }
        public int NrordemAlternativa { get; set; }
        public string Flcorreta { get; set; }

        public Questao QuestaoFkNavigation { get; set; }
    }
}
