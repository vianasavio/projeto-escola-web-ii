﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace controleEscola.Models
{
    public partial class ContextoEscola : DbContext
    {
        public ContextoEscola()
        {
        }

        public ContextoEscola(DbContextOptions<ContextoEscola> options)
            : base(options)
        {
        }

        public virtual DbSet<Alternativa> Alternativa { get; set; }
        public virtual DbSet<AlunoAleatorio> AlunoAleatorio { get; set; }
        public virtual DbSet<Curso> Curso { get; set; }
        public virtual DbSet<Disciplina> Disciplina { get; set; }
        public virtual DbSet<DisciplinaWizart> DisciplinaWizart { get; set; }
        public virtual DbSet<NivelCurso> NivelCurso { get; set; }
        public virtual DbSet<NivelDificuldadeQuestao> NivelDificuldadeQuestao { get; set; }
        public virtual DbSet<NivelDificuldadeWizart> NivelDificuldadeWizart { get; set; }
        public virtual DbSet<ParticipaoAluno> ParticipaoAluno { get; set; }
        public virtual DbSet<Pessoa> Pessoa { get; set; }
        public virtual DbSet<Questao> Questao { get; set; }
        public virtual DbSet<QuestaoCompartilhada> QuestaoCompartilhada { get; set; }
        public virtual DbSet<Questionario> Questionario { get; set; }
        public virtual DbSet<QuestionarioGerado> QuestionarioGerado { get; set; }
        public virtual DbSet<Resposta> Resposta { get; set; }
        public virtual DbSet<TipoQuestao> TipoQuestao { get; set; }
        public virtual DbSet<TipoQuestaoWizart> TipoQuestaoWizart { get; set; }
        public virtual DbSet<Turma> Turma { get; set; }
        public virtual DbSet<TurmaAluno> TurmaAluno { get; set; }
        public virtual DbSet<UsuarioEscola> UsuarioEscola { get; set; }
        public virtual DbSet<Wizard> Wizard { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
                IConfiguration Configuration = builder.Build();
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alternativa>(entity =>
            {
                entity.Property(e => e.Flcorreta)
                    .IsRequired()
                    .HasColumnName("FLCorreta")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.NrordemAlternativa).HasColumnName("NROrdemAlternativa");

                entity.Property(e => e.Txalternativa)
                    .IsRequired()
                    .HasColumnName("TXAlternativa")
                    .HasColumnType("text");

                entity.HasOne(d => d.QuestaoFkNavigation)
                    .WithMany(p => p.Alternativa)
                    .HasForeignKey(d => d.QuestaoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("QuestaoAlternativa___fk");
            });

            modelBuilder.Entity<AlunoAleatorio>(entity =>
            {
                entity.Property(e => e.DtultimaGeracao)
                    .HasColumnName("DTUltimaGeracao")
                    .HasColumnType("datetime");

                entity.Property(e => e.FlbloqueioSorteio)
                    .IsRequired()
                    .HasColumnName("FLBloqueioSorteio")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.NrrodadaAtual).HasColumnName("NRRodadaAtual");

                entity.Property(e => e.Presente)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.TurmaAlunoFkNavigation)
                    .WithMany(p => p.AlunoAleatorio)
                    .HasForeignKey(d => d.TurmaAlunoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AlunoAleatorio_TurmaAluno");
            });

            modelBuilder.Entity<Curso>(entity =>
            {
                entity.Property(e => e.Dscurso)
                    .HasColumnName("DSCurso")
                    .HasColumnType("text");

                entity.Property(e => e.Nmcurso)
                    .IsRequired()
                    .HasColumnName("NMCurso")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.NivelCursoFkNavigation)
                    .WithMany(p => p.Curso)
                    .HasForeignKey(d => d.NivelCursoFk)
                    .HasConstraintName("FK_Curso_NivelCurso");
            });

            modelBuilder.Entity<Disciplina>(entity =>
            {
                entity.Property(e => e.Cddisciplina)
                    .IsRequired()
                    .HasColumnName("CDDisciplina")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Dsdisciplina)
                    .HasColumnName("DSDisciplina")
                    .HasColumnType("text");

                entity.Property(e => e.Nmdisciplina)
                    .IsRequired()
                    .HasColumnName("NMDisciplina")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.CursoFkNavigation)
                    .WithMany(p => p.Disciplina)
                    .HasForeignKey(d => d.CursoFk)
                    .HasConstraintName("FK_Disciplina_Curso");
            });

            modelBuilder.Entity<DisciplinaWizart>(entity =>
            {
                entity.HasOne(d => d.DisciplinaFkNavigation)
                    .WithMany(p => p.DisciplinaWizart)
                    .HasForeignKey(d => d.DisciplinaFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DisciplinaWizartDisciplina___fk");

                entity.HasOne(d => d.WizartFkNavigation)
                    .WithMany(p => p.DisciplinaWizart)
                    .HasForeignKey(d => d.WizartFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DisciplinaWizartWizart___fk");
            });

            modelBuilder.Entity<NivelCurso>(entity =>
            {
                entity.Property(e => e.NmnivelCurso)
                    .HasColumnName("NMNivelCurso")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VlmaximaMedia)
                    .HasColumnName("VLMaximaMedia")
                    .HasColumnType("numeric(13, 2)");

                entity.Property(e => e.VlmediaAprovacao)
                    .HasColumnName("VLMediaAprovacao")
                    .HasColumnType("numeric(13, 2)");
            });

            modelBuilder.Entity<NivelDificuldadeQuestao>(entity =>
            {
                entity.Property(e => e.DcnivelDificuldade)
                    .HasColumnName("DCNivelDificuldade")
                    .HasColumnType("text");

                entity.Property(e => e.NmnivelDificuldade)
                    .IsRequired()
                    .HasColumnName("NMNivelDificuldade")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NivelDificuldadeWizart>(entity =>
            {
                entity.HasKey(e => e.NivelDificuldadeQuestionarioId);

                entity.HasOne(d => d.DisciplinaWizartFkNavigation)
                    .WithMany(p => p.NivelDificuldadeWizart)
                    .HasForeignKey(d => d.DisciplinaWizartFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("NivelDificuldadeWizartDisciplina___fk");

                entity.HasOne(d => d.NivelDificuldadeQuestaoFkNavigation)
                    .WithMany(p => p.NivelDificuldadeWizart)
                    .HasForeignKey(d => d.NivelDificuldadeQuestaoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("NivelDificuldadeWizartNivel___fk");
            });

            modelBuilder.Entity<ParticipaoAluno>(entity =>
            {
                entity.HasKey(e => e.ParticipacaoAlunoId);

                entity.Property(e => e.Dtsorteio)
                    .HasColumnName("DTSorteio")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nrrodada).HasColumnName("NRRodada");

                entity.Property(e => e.VlnotaParticipacao)
                    .HasColumnName("VLNotaParticipacao")
                    .HasColumnType("numeric(13, 2)");

                entity.HasOne(d => d.AlunoAleatorioFkNavigation)
                    .WithMany(p => p.ParticipaoAluno)
                    .HasForeignKey(d => d.AlunoAleatorioFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipaoAluno_AlunoAleatorio");
            });

            modelBuilder.Entity<Pessoa>(entity =>
            {
                entity.Property(e => e.CdcadastroFuturo)
                    .HasColumnName("CDCadastroFuturo")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CodNome)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Dtnascimento)
                    .HasColumnName("DTNascimento")
                    .HasColumnType("date");

                entity.Property(e => e.FlcdcadastroFuturoUtilizado)
                    .HasColumnName("FLCDCadastroFuturoUtilizado")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Nmpessoa)
                    .IsRequired()
                    .HasColumnName("NMPessoa")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nrcpf)
                    .HasColumnName("NRCPF")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Nrrg)
                    .HasColumnName("NRRG")
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.Sexo)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Questao>(entity =>
            {
                entity.Property(e => e.Dtatualizacao)
                    .HasColumnName("DTAtualizacao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtcriacao)
                    .HasColumnName("DTCriacao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Flativo)
                    .HasColumnName("FLAtivo")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FlpermitirCompartilhar)
                    .HasColumnName("FLPermitirCompartilhar")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.Nmquestao)
                    .IsRequired()
                    .HasColumnName("NMQuestao")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Txquestao)
                    .IsRequired()
                    .HasColumnName("TXQuestao")
                    .HasColumnType("text");

                entity.HasOne(d => d.DisciplinaFkNavigation)
                    .WithMany(p => p.Questao)
                    .HasForeignKey(d => d.DisciplinaFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DisciplinaQuestao___fk");

                entity.HasOne(d => d.NivelDificuldadeQuestaoFkNavigation)
                    .WithMany(p => p.Questao)
                    .HasForeignKey(d => d.NivelDificuldadeQuestaoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("NivelDificuldadeQuestao___fk");

                entity.HasOne(d => d.TipoQuestaoFkNavigation)
                    .WithMany(p => p.Questao)
                    .HasForeignKey(d => d.TipoQuestaoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TipoQuestao___fk");

                entity.HasOne(d => d.UsuarioEscolaFkNavigation)
                    .WithMany(p => p.Questao)
                    .HasForeignKey(d => d.UsuarioEscolaFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("UsuarioEscolaQuestao___fk");
            });

            modelBuilder.Entity<QuestaoCompartilhada>(entity =>
            {
                entity.Property(e => e.Dtatualizacao)
                    .HasColumnName("DTAtualizacao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtcriacao)
                    .HasColumnName("DTCriacao")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.QuetaoFkNavigation)
                    .WithMany(p => p.QuestaoCompartilhada)
                    .HasForeignKey(d => d.QuetaoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("QuestaoCompartilhada_Questao_QuestaoId_fk");

                entity.HasOne(d => d.UsuarioCompartilhadoFkNavigation)
                    .WithMany(p => p.QuestaoCompartilhada)
                    .HasForeignKey(d => d.UsuarioCompartilhadoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("QuestaoCompartilhadaUsuario___fk");
            });

            modelBuilder.Entity<Questionario>(entity =>
            {
                entity.Property(e => e.Dsquestionario)
                    .HasColumnName("DSQuestionario")
                    .HasColumnType("text");

                entity.Property(e => e.Dtatualizacao)
                    .HasColumnName("DTAtualizacao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtcriacao)
                    .IsRequired()
                    .HasColumnName("DTCriacao")
                    .IsRowVersion();

                entity.Property(e => e.FlAtivo)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Nmtitulo)
                    .HasColumnName("NMTitulo")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.UsuarioEscolaFkNavigation)
                    .WithMany(p => p.Questionario)
                    .HasForeignKey(d => d.UsuarioEscolaFk)
                    .HasConstraintName("UsuarioEscolaQuestionario___fk");
            });

            modelBuilder.Entity<QuestionarioGerado>(entity =>
            {
                entity.Property(e => e.Dtatualizacao)
                    .HasColumnName("DTAtualizacao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtcriacao)
                    .HasColumnName("DTCriacao")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.QuestaoFkNavigation)
                    .WithMany(p => p.QuestionarioGerado)
                    .HasForeignKey(d => d.QuestaoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("QuestaoQuestionario_Questao_QuestaoId_fk");

                entity.HasOne(d => d.QuestionarioFkNavigation)
                    .WithMany(p => p.QuestionarioGerado)
                    .HasForeignKey(d => d.QuestionarioFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("QuestaoQuestionario_Questionario_QuestionarioId_fk");
            });

            modelBuilder.Entity<Resposta>(entity =>
            {
                entity.Property(e => e.Txresposta)
                    .IsRequired()
                    .HasColumnName("TXResposta")
                    .HasColumnType("text");

                entity.HasOne(d => d.QuestaoFkNavigation)
                    .WithMany(p => p.Resposta)
                    .HasForeignKey(d => d.QuestaoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("QuestaoResposta___fk");
            });

            modelBuilder.Entity<TipoQuestao>(entity =>
            {
                entity.Property(e => e.DctipoQuestao)
                    .HasColumnName("DCTipoQuestao")
                    .HasColumnType("text");

                entity.Property(e => e.NmtipoQuestao)
                    .IsRequired()
                    .HasColumnName("NMTipoQuestao")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TipoQuestaoWizart>(entity =>
            {
                entity.HasKey(e => e.TipoQuestaoQuestionarioId);

                entity.Property(e => e.Qtquestoes).HasColumnName("QTQuestoes");

                entity.HasOne(d => d.TipoQuestaoFkNavigation)
                    .WithMany(p => p.TipoQuestaoWizart)
                    .HasForeignKey(d => d.TipoQuestaoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TipoQuestaoWizartTPQuestao___fk");

                entity.HasOne(d => d.WizartFkNavigation)
                    .WithMany(p => p.TipoQuestaoWizart)
                    .HasForeignKey(d => d.WizartFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TipoQuestaoWizart_Wizard_WizardId_fk");
            });

            modelBuilder.Entity<Turma>(entity =>
            {
                entity.Property(e => e.Nome)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.QtclicloAvaliacao).HasColumnName("QTClicloAvaliacao");

                entity.Property(e => e.TpclicloAvaliacao)
                    .IsRequired()
                    .HasColumnName("TPClicloAvaliacao")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.DisciplinaFkNavigation)
                    .WithMany(p => p.Turma)
                    .HasForeignKey(d => d.DisciplinaFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Turma_Disciplina");

                entity.HasOne(d => d.ProfessorFkNavigation)
                    .WithMany(p => p.Turma)
                    .HasForeignKey(d => d.ProfessorFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Turma_UsuarioEscola");
            });

            modelBuilder.Entity<TurmaAluno>(entity =>
            {
                entity.Property(e => e.NrclicloAvaliacao).HasColumnName("NRClicloAvaliacao");

                entity.HasOne(d => d.AlunoFkNavigation)
                    .WithMany(p => p.TurmaAluno)
                    .HasForeignKey(d => d.AlunoFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TurmaAluno_UsuarioEscola");

                entity.HasOne(d => d.TurmaFkNavigation)
                    .WithMany(p => p.TurmaAluno)
                    .HasForeignKey(d => d.TurmaFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TurmaAluno_Turma");
            });

            modelBuilder.Entity<UsuarioEscola>(entity =>
            {
                entity.Property(e => e.Flativo)
                    .IsRequired()
                    .HasColumnName("FLAtivo")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Prontuario)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TpusuarioEscola)
                    .IsRequired()
                    .HasColumnName("TPUsuarioEscola")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.PessoaFkNavigation)
                    .WithMany(p => p.UsuarioEscola)
                    .HasForeignKey(d => d.PessoaFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsuarioEscola_Pessoa");
            });

            modelBuilder.Entity<Wizard>(entity =>
            {
                entity.Property(e => e.Dswizart)
                    .HasColumnName("DSWizart")
                    .HasColumnType("text");

                entity.Property(e => e.Dtatualizacao)
                    .HasColumnName("DTAtualizacao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtcriacao)
                    .HasColumnName("DTCriacao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nmtitulo)
                    .HasColumnName("NMTitulo")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.QtquestoesTotal).HasColumnName("QTQuestoesTotal");

                entity.HasOne(d => d.UsuarioEscolaFkNavigation)
                    .WithMany(p => p.Wizard)
                    .HasForeignKey(d => d.UsuarioEscolaFk)
                    .HasConstraintName("WizardUsuario___fk");
            });
        }
    }
}
