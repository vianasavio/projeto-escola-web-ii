﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class TipoQuestao
    {
        public TipoQuestao()
        {
            Questao = new HashSet<Questao>();
            TipoQuestaoWizart = new HashSet<TipoQuestaoWizart>();
        }

        public int TipoQuestaoId { get; set; }
        public string NmtipoQuestao { get; set; }
        public string DctipoQuestao { get; set; }

        public ICollection<Questao> Questao { get; set; }
        public ICollection<TipoQuestaoWizart> TipoQuestaoWizart { get; set; }
    }
}
