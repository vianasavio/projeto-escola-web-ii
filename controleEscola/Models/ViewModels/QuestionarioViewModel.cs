using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class QuestionarioViewModel
    {
        public QuestionarioViewModel()
        {
            
        }
        public QuestionarioViewModel(Questionario questionario, 
                                    List<QuestionarioGerado> questionarioGerados,
                                    List<Questao> questoes)
        {
            this.Questionario = questionario;
            this.QuestionarioGerados = questionarioGerados;
            this.Questoes = questoes;
        }
        
        public Questionario Questionario {get; set;}
        public List<QuestionarioGerado> QuestionarioGerados {get; set;}
        public List<Questao> Questoes {get; set;}

    }
}
