using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace controleEscola.Models
{
    public partial class WizardViewModel
    {
        private readonly ContextoEscola _contexto;
        public int? UsuarioEscolaFk { get; set; }
        [Display(Name="Disciplina"), Required]
        public int? DisciplinaFk { get; set; }
        [Display(Name="Total de Questões"), Required]
        [Range(1, int.MaxValue, ErrorMessage="O total de questões deve ser maior que 0")]
        public int QtquestoesTotal { get; set; }
        [Display(Name="Titulo do Questionario"), Required]
        public string Nmtitulo { get; set; }
        [Display(Name="Descrição do Questionario"), Required]
        public string Dsquestionario { get; set; }
        public DateTime? Dtcriacao { get; set; }
        public DateTime? Dtatualizacao { get; set; }

        public UsuarioEscola UsuarioEscolaFkNavigation { get; set; }
        public ICollection<DisciplinaWizart> DisciplinaWizart { get; set; }
        public ICollection<TipoQuestaoWizart> TipoQuestaoWizart { get; set; }


        public List<Disciplina> Disciplinas {get; set;}

        public List<UsuarioEscola> usuarios {get; set;}

        public List<TipoQuestao> TipoQuestoes {get; set;}

        public List<NivelDificuldadeQuestao> NivelDificuldadeQuestoes {get; set;}
    
        public List<QuantidadeTipoQuestao> QuantidadeTipoQuestoes {get; set;}

        [Display(Name="Niveis de dificuldade"), Required]
        [EnsureOneElement(ErrorMessage = "At least a Nivel is required")]
        public List<int> NivelDificuldadeQuestoesFk {get; set;}

        public WizardViewModel()
        {
            _contexto = new ContextoEscola();

            Disciplinas = _contexto.Disciplina.ToList();
            TipoQuestoes = _contexto.TipoQuestao.ToList();
            NivelDificuldadeQuestoes = _contexto.NivelDificuldadeQuestao.ToList();
        }
    
    }

    public class QuantidadeTipoQuestao{

        public QuantidadeTipoQuestao()
        {

        }
        public int tipoId {get; set;}
        [Required]
        public int Qtdtipo {get; set;}
    }

    //validação personalizada
    public class EnsureOneElementAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var list = value as IList;
            if (list != null)
            {
                return list.Count > 0;
            }
            return false;
        }
    }



}
