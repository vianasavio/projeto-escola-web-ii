using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace controleEscola.Models
{
    public partial class DisciplinaViewModel
    {
        public DisciplinaViewModel(Disciplina disciplina)
        {
            DisciplinaId = disciplina.DisciplinaId;
            CursoFk = disciplina.CursoFk;
            Nmdisciplina = disciplina.Nmdisciplina;
            Cddisciplina = disciplina.Cddisciplina;
            Dsdisciplina = disciplina.Dsdisciplina;
        }

        public DisciplinaViewModel()
        {

        }

        public int DisciplinaId { get; set; }
        [Display(Name="Nome do Curso"), Required]
        public int? CursoFk { get; set; }
        [Display(Name="Nome"), Required]
        public string Nmdisciplina { get; set; }
        [Display(Name="Codigo"), Required]
        public string Cddisciplina { get; set; }
        [Display(Name="Descrição"), Required]
        public string Dsdisciplina { get; set; }

        public Curso CursoFkNavigation { get; set; }
        public List<DisciplinaWizart> DisciplinaWizart { get; set; }
        public List<Questao> Questao { get; set; }
        public List<Turma> Turma { get; set; }

        public List<Curso> Cursos { get; set; }
    }
}

