﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace controleEscola.Models.ViewModels
{
    public class UsuarioEscolaPessoaViewModel : Controller
    {
        // GET: UsuarioEscolaPessoaViewModel
        public ActionResult Index()
        {
            return View();
        }

        // GET: UsuarioEscolaPessoaViewModel/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UsuarioEscolaPessoaViewModel/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UsuarioEscolaPessoaViewModel/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UsuarioEscolaPessoaViewModel/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: UsuarioEscolaPessoaViewModel/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UsuarioEscolaPessoaViewModel/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: UsuarioEscolaPessoaViewModel/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}