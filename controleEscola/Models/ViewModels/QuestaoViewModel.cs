using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace controleEscola.Models
{
    public partial class QuestaoViewModel
    {
        private readonly ContextoEscola _contexto;
        public QuestaoViewModel(){
            
        }

        public QuestaoViewModel(Questao questao)
        {
            _contexto = new ContextoEscola();

            QuestaoId = questao.QuestaoId;
            Nmquestao = questao.Nmquestao;
            Txquestao = questao.Txquestao;
            DisciplinaFk = questao.DisciplinaFk;
            UsuarioEscolaFk = questao.UsuarioEscolaFk;
            TipoQuestaoFk = questao.TipoQuestaoFk;
            NivelDificuldadeQuestaoFk = questao.NivelDificuldadeQuestaoFk;
            FlpermitirCompartilhar = questao.FlpermitirCompartilhar;
            Dtcriacao = questao.Dtcriacao;
            Dtatualizacao = questao.Dtatualizacao;
            
            Resposta = questao.Resposta.ToList();
            Alternativa = questao.Alternativa.ToList();
            
            Disciplina = _contexto.Disciplina.ToList();
            UsuarioEscola = _contexto.UsuarioEscola.ToList();
            TipoQuestao = _contexto.TipoQuestao.ToList();
            NivelDificuldadeQuestao = _contexto.NivelDificuldadeQuestao.ToList();
        }
        
        
        public int QuestaoId { get; set; }
        [Display(Name="Nome da Questão"), Required]
        public string Nmquestao { get; set; }
        [Display(Name="Descrição da Questão"), Required]
        public string Txquestao { get; set; }
        [Display(Name="Nome da Disciplina"), Required]
        public int DisciplinaFk { get; set; }
        [Display(Name="Usuario"), Required]
        public int UsuarioEscolaFk { get; set; }
        [Display(Name="Tipo"), Required]
        public int TipoQuestaoFk { get; set; }
        [Display(Name="Nivel dificuldade"), Required]
        public int NivelDificuldadeQuestaoFk { get; set; }
        [Display(Name="Compartilhar"), Required]
        public string FlpermitirCompartilhar { get; set; }
        public DateTime Dtcriacao { get; set; }
        public DateTime? Dtatualizacao { get; set; }
        public string Flativo { get; set; }

        //public int AlternativaCorreta {get; set;}

        public List<Alternativa> Alternativa {get; set;}
        public List<Resposta> Resposta {get; set;}

        public List<Disciplina> Disciplina {get; set;}
        public List<UsuarioEscola> UsuarioEscola {get; set;}

        public List<TipoQuestao> TipoQuestao {get; set;}
        public List<NivelDificuldadeQuestao> NivelDificuldadeQuestao {get; set;}
    }
}
