﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class DisciplinaWizart
    {
        public DisciplinaWizart()
        {
            NivelDificuldadeWizart = new HashSet<NivelDificuldadeWizart>();
        }

        public int DisciplinaWizartId { get; set; }
        public int DisciplinaFk { get; set; }
        public int WizartFk { get; set; }

        public Disciplina DisciplinaFkNavigation { get; set; }
        public Wizard WizartFkNavigation { get; set; }
        public ICollection<NivelDificuldadeWizart> NivelDificuldadeWizart { get; set; }
    }
}
