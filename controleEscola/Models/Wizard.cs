﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class Wizard
    {
        public Wizard()
        {
            DisciplinaWizart = new HashSet<DisciplinaWizart>();
            TipoQuestaoWizart = new HashSet<TipoQuestaoWizart>();
        }

        public int WizardId { get; set; }
        public int? UsuarioEscolaFk { get; set; }
        public int QtquestoesTotal { get; set; }
        public string Nmtitulo { get; set; }
        public string Dswizart { get; set; }
        public DateTime? Dtcriacao { get; set; }
        public DateTime? Dtatualizacao { get; set; }

        public UsuarioEscola UsuarioEscolaFkNavigation { get; set; }
        public ICollection<DisciplinaWizart> DisciplinaWizart { get; set; }
        public ICollection<TipoQuestaoWizart> TipoQuestaoWizart { get; set; }
    }
}
