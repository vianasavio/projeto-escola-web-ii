﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace controleEscola.Models
{
    public partial class Questao
    {
        public Questao()
        {
            Alternativa = new HashSet<Alternativa>();
            QuestaoCompartilhada = new HashSet<QuestaoCompartilhada>();
            QuestionarioGerado = new HashSet<QuestionarioGerado>();
            Resposta = new HashSet<Resposta>();
        }

        public Questao(int id, string nome, string texto, int fkDisciplina, int usuarioEscolaFk, 
                        int tipoQuestaoFk, int nivelDificuldadeQuestaoFk, string flpermitirCompartilhar)
        {
            QuestaoId = id;
            Nmquestao =nome;
            Txquestao = texto;
            DisciplinaFk = fkDisciplina;
            UsuarioEscolaFk = usuarioEscolaFk;
            TipoQuestaoFk = tipoQuestaoFk;
            NivelDificuldadeQuestaoFk = nivelDificuldadeQuestaoFk;
            FlpermitirCompartilhar = flpermitirCompartilhar;
            Dtatualizacao = DateTime.Now;
            Dtcriacao = DateTime.Now;
            Flativo = "A";
        }

        public int QuestaoId { get; set; }
        [Display(Name="Nome da Questão"), Required]
        public string Nmquestao { get; set; }
        [Display(Name="Descrição da Questão"), Required]
        public string Txquestao { get; set; }
        [Display(Name="Disciplina"), Required]
        public int DisciplinaFk { get; set; }
        [Display(Name="Usuario"), Required]
        public int UsuarioEscolaFk { get; set; }
        [Display(Name="Tipo"), Required]
        public int TipoQuestaoFk { get; set; }
        [Display(Name="Nivel dificuldade"), Required]
        public int NivelDificuldadeQuestaoFk { get; set; }
        [Display(Name="Compartilhar"), Required]
        public string FlpermitirCompartilhar { get; set; }
        [Display(Name="Data Criação"), Required]
        public DateTime Dtcriacao { get; set; }
        [Display(Name="Data Atualização"), Required]
        public DateTime? Dtatualizacao { get; set; }
        [Display(Name="Ativo"), Required]
        public string Flativo { get; set; }

        public Disciplina DisciplinaFkNavigation { get; set; }
        public NivelDificuldadeQuestao NivelDificuldadeQuestaoFkNavigation { get; set; }
        public TipoQuestao TipoQuestaoFkNavigation { get; set; }
        public UsuarioEscola UsuarioEscolaFkNavigation { get; set; }
        public ICollection<Alternativa> Alternativa { get; set; }
        public ICollection<QuestaoCompartilhada> QuestaoCompartilhada { get; set; }
        public ICollection<QuestionarioGerado> QuestionarioGerado { get; set; }
        public ICollection<Resposta> Resposta { get; set; }
    

        public int SaveQuestao()
        {
            var _context = new ContextoEscola();
            if (QuestaoId > 0){
                _context.Questao.Update(this);
            }else{
                _context.Questao.Add(this);
            }
            
            _context.SaveChanges();
    
            return this.QuestaoId;
        }//Insere ou atualiza uma questão

        public Questao getQuestao(int id)
        {
            var _context = new ContextoEscola();
            return  _context.Questao
                            .Include(q => q.DisciplinaFkNavigation)
                            .Include(u => u.UsuarioEscolaFkNavigation)
                                .ThenInclude(p => p.PessoaFkNavigation)
                            .Include(t => t.TipoQuestaoFkNavigation)
                            .Include(n => n.NivelDificuldadeQuestaoFkNavigation)
                            .Include(r => r.Resposta)
                            .Include(a => a.Alternativa)
                            .Where(qt => qt.QuestaoId == id).SingleOrDefault();
        }//busca e retorna uma questão especifica

        public List<Questao> getAllQuestao()
        {
            var _context = new ContextoEscola();
            return _context.Questao
                            .Include(q => q.DisciplinaFkNavigation)
                            .Include(u => u.UsuarioEscolaFkNavigation)
                                .ThenInclude(p => p.PessoaFkNavigation)
                            .Include(t => t.TipoQuestaoFkNavigation)
                            .Include(n => n.NivelDificuldadeQuestaoFkNavigation)
                                .ToList();
        }//retorna uma listar de todas as questões cadastradas
    
    }
}
