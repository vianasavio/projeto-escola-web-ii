﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class AlunoAleatorio
    {
        public AlunoAleatorio()
        {
            ParticipaoAluno = new HashSet<ParticipaoAluno>();
        }

        public int AlunoAleatorioId { get; set; }
        public int TurmaAlunoFk { get; set; }
        public string FlbloqueioSorteio { get; set; }
        public short? NrrodadaAtual { get; set; }
        public DateTime? DtultimaGeracao { get; set; }
        public string Presente { get; set; }

        public TurmaAluno TurmaAlunoFkNavigation { get; set; }
        public ICollection<ParticipaoAluno> ParticipaoAluno { get; set; }
    }
}
