﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class NivelDificuldadeWizart
    {
        public int NivelDificuldadeQuestionarioId { get; set; }
        public int NivelDificuldadeQuestaoFk { get; set; }
        public int DisciplinaWizartFk { get; set; }

        public DisciplinaWizart DisciplinaWizartFkNavigation { get; set; }
        public NivelDificuldadeQuestao NivelDificuldadeQuestaoFkNavigation { get; set; }
    }
}
