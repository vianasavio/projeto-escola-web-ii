﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class QuestaoCompartilhada
    {
        public int QuestaoCompartilhadaId { get; set; }
        public int QuetaoFk { get; set; }
        public int UsuarioCompartilhadoFk { get; set; }
        public DateTime Dtcriacao { get; set; }
        public DateTime? Dtatualizacao { get; set; }

        public Questao QuetaoFkNavigation { get; set; }
        public UsuarioEscola UsuarioCompartilhadoFkNavigation { get; set; }
    }
}
