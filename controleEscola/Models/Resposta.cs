﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class Resposta
    {
        public int RespostaId { get; set; }
        public int QuestaoFk { get; set; }
        public string Txresposta { get; set; }

        public Questao QuestaoFkNavigation { get; set; }
    }
}
