﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class Turma
    {
        public Turma()
        {
            TurmaAluno = new HashSet<TurmaAluno>();
        }

        public int TurmaId { get; set; }
        public int DisciplinaFk { get; set; }
        public int ProfessorFk { get; set; }
        public string TpclicloAvaliacao { get; set; }
        public short QtclicloAvaliacao { get; set; }
        public short Ano { get; set; }
        public string Nome { get; set; }

        public Disciplina DisciplinaFkNavigation { get; set; }
        public UsuarioEscola ProfessorFkNavigation { get; set; }
        public ICollection<TurmaAluno> TurmaAluno { get; set; }
    }
}
