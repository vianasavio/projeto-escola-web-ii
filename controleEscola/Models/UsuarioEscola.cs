﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class UsuarioEscola
    {
        public UsuarioEscola()
        {
            Questao = new HashSet<Questao>();
            QuestaoCompartilhada = new HashSet<QuestaoCompartilhada>();
            Questionario = new HashSet<Questionario>();
            Turma = new HashSet<Turma>();
            TurmaAluno = new HashSet<TurmaAluno>();
            Wizard = new HashSet<Wizard>();
        }

        public int UsuarioEscolaId { get; set; }
        public int PessoaFk { get; set; }
        public string Prontuario { get; set; }
        public string TpusuarioEscola { get; set; }
        public string Flativo { get; set; }

        public Pessoa PessoaFkNavigation { get; set; }
        public ICollection<Questao> Questao { get; set; }
        public ICollection<QuestaoCompartilhada> QuestaoCompartilhada { get; set; }
        public ICollection<Questionario> Questionario { get; set; }
        public ICollection<Turma> Turma { get; set; }
        public ICollection<TurmaAluno> TurmaAluno { get; set; }
        public ICollection<Wizard> Wizard { get; set; }
    }
}
