﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace controleEscola.Models
{
    public partial class Disciplina
    {
        public Disciplina()
        {
            DisciplinaWizart = new HashSet<DisciplinaWizart>();
            Questao = new HashSet<Questao>();
            Turma = new HashSet<Turma>();
        }

        [Display(Name="Id")]
        public int DisciplinaId { get; set; }
        [Display(Name="Curso")]
        public int? CursoFk { get; set; }
        [Display(Name="Nome")]
        public string Nmdisciplina { get; set; }
        [Display(Name="Codigo")]
        public string Cddisciplina { get; set; }
        [Display(Name="Descrição")]
        public string Dsdisciplina { get; set; }

        public Curso CursoFkNavigation { get; set; }
        public ICollection<DisciplinaWizart> DisciplinaWizart { get; set; }
        public ICollection<Questao> Questao { get; set; }
        public ICollection<Turma> Turma { get; set; }
    }
}
