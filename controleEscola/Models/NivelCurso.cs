﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class NivelCurso
    {
        public NivelCurso()
        {
            Curso = new HashSet<Curso>();
        }

        public int NivelCursoId { get; set; }
        public string NmnivelCurso { get; set; }
        public decimal? VlmediaAprovacao { get; set; }
        public decimal? VlmaximaMedia { get; set; }

        public ICollection<Curso> Curso { get; set; }
    }
}
