﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class NivelDificuldadeQuestao
    {
        public NivelDificuldadeQuestao()
        {
            NivelDificuldadeWizart = new HashSet<NivelDificuldadeWizart>();
            Questao = new HashSet<Questao>();
        }

        public int NivelDificuldadeQuestaoId { get; set; }
        public string NmnivelDificuldade { get; set; }
        public string DcnivelDificuldade { get; set; }

        public ICollection<NivelDificuldadeWizart> NivelDificuldadeWizart { get; set; }
        public ICollection<Questao> Questao { get; set; }
    }
}
