﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class TipoQuestaoWizart
    {
        public int TipoQuestaoQuestionarioId { get; set; }
        public int WizartFk { get; set; }
        public int TipoQuestaoFk { get; set; }
        public int Qtquestoes { get; set; }

        public TipoQuestao TipoQuestaoFkNavigation { get; set; }
        public Wizard WizartFkNavigation { get; set; }
    }
}
