﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class TurmaAluno
    {
        public TurmaAluno()
        {
            AlunoAleatorio = new HashSet<AlunoAleatorio>();
        }

        public int TurmaAlunoId { get; set; }
        public int TurmaFk { get; set; }
        public int AlunoFk { get; set; }
        public short? NrclicloAvaliacao { get; set; }

        public UsuarioEscola AlunoFkNavigation { get; set; }
        public Turma TurmaFkNavigation { get; set; }
        public ICollection<AlunoAleatorio> AlunoAleatorio { get; set; }
    }
}
