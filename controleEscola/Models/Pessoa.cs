﻿using System;
using System.Collections.Generic;

namespace controleEscola.Models
{
    public partial class Pessoa
    {
        public Pessoa()
        {
            UsuarioEscola = new HashSet<UsuarioEscola>();
        }

        public int PessoaId { get; set; }
        public string Nmpessoa { get; set; }
        public string CodNome { get; set; }
        public string Nrcpf { get; set; }
        public string Nrrg { get; set; }
        public string Sexo { get; set; }
        public DateTime? Dtnascimento { get; set; }
        public string CdcadastroFuturo { get; set; }
        public string FlcdcadastroFuturoUtilizado { get; set; }

        public ICollection<UsuarioEscola> UsuarioEscola { get; set; }
    }
}
