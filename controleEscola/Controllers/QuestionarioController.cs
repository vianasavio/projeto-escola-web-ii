using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using controleEscola.Models;
using System.Globalization;
using Microsoft.EntityFrameworkCore;

namespace controleEscola.Controllers
{
    public class QuestionarioController : Controller
    {
        private readonly ContextoEscola _contexto;
        public QuestionarioController()
        {
            _contexto = new ContextoEscola();
        }
        public IActionResult Index(){
            var model = _contexto.Questionario.ToList();
            return View(model);
        }   
        
        public IActionResult Mostrar(int id)
        {
            Questionario questionario = _contexto.Questionario
                            .Where(q => q.QuestionarioId == id).SingleOrDefault();
            
            List<QuestionarioGerado> questionarioGeradoList = _contexto.QuestionarioGerado.Where(qg => qg.QuestionarioFk == id).ToList();
            
            List<int> questoesId = new List<int>(); 
            foreach (var item in questionarioGeradoList)
                questoesId.Add(item.QuestaoFk);

            var IQquestoes = from questao in _contexto.Questao 
                    where questoesId.ToArray().Contains(questao.QuestaoId)
                    select questao;

            List<Questao> questoes = IQquestoes.ToList();
            foreach (var q in questoes )
            {
                q.Alternativa = _contexto.Alternativa.Where(a => a.QuestaoFk == q.QuestaoId).ToList();
                q.Resposta = _contexto.Resposta.Where(r => r.QuestaoFk == q.QuestaoId).ToList();
            }
            QuestionarioViewModel questionarioViewModel = new QuestionarioViewModel(questionario, 
                                                                                    questionarioGeradoList, 
                                                                                    questoes);

            return View(questionarioViewModel);
        }
        
        public IActionResult GerarQuestionario(){
            
            var wizart = new WizardViewModel();
            return View(wizart);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult GerarQuestionario(WizardViewModel wizardView)
        { 
            if (ModelState.IsValid)
            {
                List<int> tipoQuestoesFk = new List<int>();
                foreach (var tp in wizardView.QuantidadeTipoQuestoes)
                {
                    if (tp.Qtdtipo > 0){
                        tipoQuestoesFk.Add(tp.tipoId);
                    }
                }
                
                var matches = from questao in _contexto.Questao 
                    where questao.DisciplinaFk == wizardView.DisciplinaFk
                    where  wizardView.NivelDificuldadeQuestoesFk.ToArray().Contains(questao.NivelDificuldadeQuestaoFk) 
                    where tipoQuestoesFk.ToArray().Contains(questao.TipoQuestaoFk)
                    select questao;

                List<Questao> minhasQuestoes = matches.ToList(); //todas as questões possiveis para sorteio
                
                int qtdQuestoes = minhasQuestoes.Count(); //quantidade de questões possiveis para seleção
                
                List<Questao> questoesSelecionada = new List<Questao>(); //lista de questões selecionada

                int count = 0;
                while(count < wizardView.QtquestoesTotal)
                {
                    Random random = new Random();
                    int indice = random.Next(minhasQuestoes.Count());
                    
                    var questaoAtual = minhasQuestoes[indice]; //verifica se a questão ja existe na lista
                    
                    if(questaoAtual == null){
                        continue;
                    }else{
                        
                        foreach (var tp in wizardView.QuantidadeTipoQuestoes)
                        {
                            if (questaoAtual.TipoQuestaoFk == tp.tipoId)
                            {
                                int qtdTipoSelecionado = questoesSelecionada.FindAll(t => t.TipoQuestaoFk == tp.tipoId).Count();
                                
                                if (qtdTipoSelecionado >= tp.Qtdtipo)
                                {
                                    continue;
                                }else{
                                    questoesSelecionada.Add(questaoAtual);
                                    minhasQuestoes.Remove(questaoAtual);
                                }
                            }//verifico tipo da questão
                        }
                        
                    }//caso a questão ainda não foi selecionada
                    
                    count = count +1;
                }//loop para gerar as questões
            
                List<Questao> listaQuestaoFinal = questoesSelecionada;

                Questionario questionarioNovo = new Questionario{
                    Nmtitulo =  wizardView.Nmtitulo,
                    Dsquestionario = wizardView.Dsquestionario,
                    Dtcriacao = BitConverter.GetBytes(DateTime.Now.Ticks),
                    Dtatualizacao = DateTime.Now,
                    FlAtivo = "A",
                };

                _contexto.Questionario.Add(questionarioNovo);
                _contexto.SaveChanges();

                foreach (var item in questoesSelecionada)
                {
                    QuestionarioGerado questionarioGerado = new QuestionarioGerado{
                        QuestionarioFk = questionarioNovo.QuestionarioId,
                        QuestaoFk = item.QuestaoId,
                        Dtcriacao = DateTime.Now,
                        Dtatualizacao = DateTime.Now
                    };

                    _contexto.QuestionarioGerado.Add(questionarioGerado);
                    _contexto.SaveChanges();
                }

                return RedirectToAction("Index");
            }else{
                return View(wizardView);
            }
        }
        
    }
}