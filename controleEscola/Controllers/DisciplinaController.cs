using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using controleEscola.Models;
using System.Globalization;
using Microsoft.EntityFrameworkCore;

namespace controleEscola.Controllers
{
    public class DisciplinaController : Controller
    {

        private ContextoEscola _contexto;

        public DisciplinaController(){
            _contexto = new ContextoEscola();
        }


        public IActionResult Index()
        {
            var disciplinas = _contexto.Disciplina
                        .Include(c => c.CursoFkNavigation)
                        .ToList();
            return View(disciplinas);
        }

        public IActionResult Mostrar(int id)
        {
            var disciplina = _contexto.Disciplina.Where(d => d.DisciplinaId == id)
                        .Include(c => c.CursoFkNavigation)
                        .ToList().SingleOrDefault();

            if (disciplina != null)
            {
                return View(disciplina);
            }else{
                return NotFound();
            }
        }

        public IActionResult Cadastrar()
        {
            var cursos = _contexto.Curso.ToList();
            var model = new DisciplinaViewModel(new Disciplina());
            model.Cursos = cursos;
            
            return View(model);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Cadastrar(DisciplinaViewModel disciplinaView)
        {
            if (ModelState.IsValid)
            {
                var disciplina = new Disciplina{
                    Nmdisciplina = disciplinaView.Nmdisciplina,
                    Cddisciplina = disciplinaView.Cddisciplina,
                    Dsdisciplina = disciplinaView.Dsdisciplina,
                    CursoFk = disciplinaView.CursoFk,
                };

                _contexto.Disciplina.Add(disciplina);
                _contexto.SaveChanges();
                
                return RedirectToAction("Index");
            }else
            {
                var model = new DisciplinaViewModel(new Disciplina());
                return View(model);
            }
        }//cadastrar Disciplina

        public IActionResult Editar(int id)
        {   
            var disciplina =  _contexto.Disciplina.Where(d => d.DisciplinaId == id).SingleOrDefault();
            var disciplinaView = new DisciplinaViewModel(disciplina);
            disciplinaView.Cursos = _contexto.Curso.ToList();
            if (disciplinaView != null)
            { 
                return View(disciplinaView);
            }else{
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Editar(DisciplinaViewModel disciplinaView)
        {
            if (ModelState.IsValid)
            {
                var disciplina = new Disciplina{
                    DisciplinaId = disciplinaView.DisciplinaId,
                    Nmdisciplina = disciplinaView.Nmdisciplina,
                    Cddisciplina = disciplinaView.Cddisciplina,
                    Dsdisciplina = disciplinaView.Dsdisciplina,
                    CursoFk = disciplinaView.CursoFk,
                };

                _contexto.Disciplina.Update(disciplina);
                _contexto.SaveChanges();

                return RedirectToAction("Index");
            }else
            {
                var model = new QuestaoViewModel(new Questao());
                return View(model);
            }
        }

        public IActionResult Deletar(int id)
        {
            var disciplina =  _contexto.Disciplina.Where(d => d.DisciplinaId == id)
                        .Include(c => c.CursoFkNavigation)
                        .ToList()
                        .SingleOrDefault();
                        
            if (disciplina != null)
            {
                return View(disciplina);
            }else{
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Deletar")]
        public IActionResult DeletarConfirmado(int id)
        {
            var disciplina =  _contexto.Disciplina.Where(d => d.DisciplinaId == id).SingleOrDefault();
            _contexto.Disciplina.Remove(disciplina);
            _contexto.SaveChanges();

            return RedirectToAction("Index");
        }

    }

}