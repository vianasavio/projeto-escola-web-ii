using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using controleEscola.Models;
using System.Globalization;
using Microsoft.EntityFrameworkCore;

namespace controleEscola.Controllers
{
    public class QuestaoController : Controller
    {
        private ContextoEscola _contexto;

        public QuestaoController(){
            _contexto = new ContextoEscola();
        }

        public IActionResult Index()
        {
            var questao = new Questao().getAllQuestao();
            return View(questao);
        }

        public IActionResult Mostrar(int id)
        {
            var questao = new Questao().getQuestao(id);
            if (questao != null)
            {
                return View(questao);
            }else{
                return NotFound();
            }
        }
        public IActionResult Cadastrar()
        {
            var model = new QuestaoViewModel(new Questao());
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Cadastrar(QuestaoViewModel questaoView)
        {
            if (ModelState.IsValid)
            {
                var questao = new Questao(questaoView.QuestaoId, questaoView.Nmquestao, questaoView.Txquestao, questaoView.DisciplinaFk, 
                                            questaoView.UsuarioEscolaFk, questaoView.TipoQuestaoFk, 
                                            questaoView.NivelDificuldadeQuestaoFk, questaoView.FlpermitirCompartilhar);

                int id = questao.SaveQuestao();

                if( questaoView.Alternativa != null )
                {
                    int i=1;
                    foreach (var alternativa in questaoView.Alternativa)
                    {
                        alternativa.QuestaoFk = id;
                        alternativa.NrordemAlternativa = i;
                        if(alternativa.Flcorreta == "S")
                        {
                            alternativa.Flcorreta = "S";
                        }else
                        {
                            alternativa.Flcorreta = "N";
                        }
                        
                        _contexto.Alternativa.Add(alternativa);
                        _contexto.SaveChanges();
                        i++;
                    }
                }//caso estiver cadastrando uma alternativa

                if (questaoView.Resposta != null )
                {
                    foreach (var respota in questaoView.Resposta)
                    {
                        respota.QuestaoFk = id;
                        _contexto.Resposta.Add(respota);
                        _contexto.SaveChanges();
                    }
                }//caso for uma resposta
                
                return RedirectToAction("Index");
            }else
            {
                var model = new QuestaoViewModel(new Questao());
                return View(model);
            }
        }//cadastrar Questão

        public IActionResult Editar(int id)
        {   
            var questao = new Questao().getQuestao(id);
            if (questao != null)
            { 
                var model = new QuestaoViewModel(questao);
                return View(model);
            }else{
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Editar(QuestaoViewModel questaoView)
        {
            if (ModelState.IsValid)
            {
                var questao = new Questao(questaoView.QuestaoId, questaoView.Nmquestao, questaoView.Txquestao, questaoView.DisciplinaFk, 
                                            questaoView.UsuarioEscolaFk, questaoView.TipoQuestaoFk, 
                                            questaoView.NivelDificuldadeQuestaoFk, questaoView.FlpermitirCompartilhar);

                int id = questao.SaveQuestao();

                if( questaoView.Alternativa != null )
                {
                    //remover alternativas
                    var alternativatoRemove = _contexto.Alternativa.ToList().Where(a => a.QuestaoFk == id );
                    _contexto.Alternativa.RemoveRange(alternativatoRemove);
                    _contexto.SaveChanges();
                    int i=1;
                    foreach (var alternativa in questaoView.Alternativa)
                    {
                        alternativa.QuestaoFk = id;
                        alternativa.NrordemAlternativa = i;
                        if(alternativa.Flcorreta == "S")
                        {
                            alternativa.Flcorreta = "S";
                        }else
                        {
                            alternativa.Flcorreta = "N";
                        }
                        
                        _contexto.Alternativa.Add(alternativa);
                        _contexto.SaveChanges();
                        i++;
                    }
                }//caso estiver cadastrando uma alternativa

                if (questaoView.Resposta != null )
                {
                    var respostaToRemove = _contexto.Resposta.ToList().Where(r => r.QuestaoFk == id);
                    _contexto.Resposta.RemoveRange(respostaToRemove);
                    _contexto.SaveChanges();
                    foreach (var respota in questaoView.Resposta)
                    {
                        respota.QuestaoFk = id;
                        _contexto.Resposta.Add(respota);
                        _contexto.SaveChanges();
                    }
                }//caso for uma resposta
                
                return RedirectToAction("Index");

            }else
            {
                var model = new QuestaoViewModel(new Questao());
                return View(model);
            }
        }

        public IActionResult Deletar(int id)
        {
            var questao = new Questao().getQuestao(id);
            if (questao != null)
            {
                return View(questao);
            }else{
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Deletar")]
        public IActionResult DeletarConfirmado(int id)
        {    
            List<Alternativa> alternativas = _contexto.Alternativa.Where(a =>a.QuestaoFk == id).ToList();
            List<Resposta> respostas = _contexto.Resposta.Where(r => r.QuestaoFk == id).ToList();
            
            if (alternativas.Count() > 0)
            {
                var deleteOrderAlternatives = (from alternativa in _contexto.Alternativa 
                    where alternativa.QuestaoFk == id
                    select alternativa);

                foreach (var item in deleteOrderAlternatives)
                {   
                    _contexto.Alternativa.Remove(item);
                }

                _contexto.SaveChanges();
            }

            if (respostas.Count() >  0)
            {
                var deleteOrderResponse = (from resposta in _contexto.Resposta 
                    where resposta.QuestaoFk == id
                    select resposta);

                foreach (var item in deleteOrderResponse)
                {   
                    _contexto.Resposta.Remove(item);
                }

                _contexto.SaveChanges();
            }
            
            
            Questao questao = new Questao().getQuestao(id);
            _contexto.Questao.Remove(questao);            
            _contexto.SaveChanges();

            
            return RedirectToAction("Index");

        }
    }
}
