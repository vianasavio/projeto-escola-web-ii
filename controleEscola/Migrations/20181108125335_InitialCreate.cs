﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace controleEscola.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NivelCurso",
                columns: table => new
                {
                    NivelCursoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NMNivelCurso = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    VLMediaAprovacao = table.Column<decimal>(type: "numeric(13, 2)", nullable: true),
                    VLMaximaMedia = table.Column<decimal>(type: "numeric(13, 2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NivelCurso", x => x.NivelCursoId);
                });

            migrationBuilder.CreateTable(
                name: "NivelDificuldadeQuestao",
                columns: table => new
                {
                    NivelDificuldadeQuestaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NMNivelDificuldade = table.Column<string>(unicode: false, maxLength: 45, nullable: false),
                    DCNivelDificuldade = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NivelDificuldadeQuestao", x => x.NivelDificuldadeQuestaoId);
                });

            migrationBuilder.CreateTable(
                name: "Pessoa",
                columns: table => new
                {
                    PessoaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NMPessoa = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    CodNome = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    NRCPF = table.Column<string>(unicode: false, maxLength: 11, nullable: true),
                    NRRG = table.Column<string>(unicode: false, maxLength: 13, nullable: true),
                    Sexo = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    DTNascimento = table.Column<DateTime>(type: "date", nullable: true),
                    CDCadastroFuturo = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    FLCDCadastroFuturoUtilizado = table.Column<string>(unicode: false, maxLength: 1, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pessoa", x => x.PessoaId);
                });

            migrationBuilder.CreateTable(
                name: "TipoQuestao",
                columns: table => new
                {
                    TipoQuestaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NMTipoQuestao = table.Column<string>(unicode: false, maxLength: 45, nullable: false),
                    DCTipoQuestao = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoQuestao", x => x.TipoQuestaoId);
                });

            migrationBuilder.CreateTable(
                name: "Curso",
                columns: table => new
                {
                    CursoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NivelCursoFk = table.Column<int>(nullable: true),
                    NMCurso = table.Column<string>(unicode: false, maxLength: 45, nullable: false),
                    DSCurso = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Curso", x => x.CursoId);
                    table.ForeignKey(
                        name: "FK_Curso_NivelCurso",
                        column: x => x.NivelCursoFk,
                        principalTable: "NivelCurso",
                        principalColumn: "NivelCursoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UsuarioEscola",
                columns: table => new
                {
                    UsuarioEscolaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PessoaFk = table.Column<int>(nullable: false),
                    Prontuario = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    TPUsuarioEscola = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    FLAtivo = table.Column<string>(unicode: false, maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuarioEscola", x => x.UsuarioEscolaId);
                    table.ForeignKey(
                        name: "FK_UsuarioEscola_Pessoa",
                        column: x => x.PessoaFk,
                        principalTable: "Pessoa",
                        principalColumn: "PessoaId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Disciplina",
                columns: table => new
                {
                    DisciplinaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CursoFk = table.Column<int>(nullable: true),
                    NMDisciplina = table.Column<string>(unicode: false, maxLength: 45, nullable: false),
                    CDDisciplina = table.Column<string>(unicode: false, maxLength: 45, nullable: false),
                    DSDisciplina = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Disciplina", x => x.DisciplinaId);
                    table.ForeignKey(
                        name: "FK_Disciplina_Curso",
                        column: x => x.CursoFk,
                        principalTable: "Curso",
                        principalColumn: "CursoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Questionario",
                columns: table => new
                {
                    QuestionarioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UsuarioEscolaFk = table.Column<int>(nullable: true),
                    NMTitulo = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    DSQuestionario = table.Column<string>(type: "text", nullable: true),
                    DTCriacao = table.Column<byte[]>(rowVersion: true, nullable: false),
                    DTAtualizacao = table.Column<DateTime>(type: "datetime", nullable: true),
                    FlAtivo = table.Column<string>(unicode: false, maxLength: 1, nullable: false, defaultValueSql: "('A')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questionario", x => x.QuestionarioId);
                    table.ForeignKey(
                        name: "UsuarioEscolaQuestionario___fk",
                        column: x => x.UsuarioEscolaFk,
                        principalTable: "UsuarioEscola",
                        principalColumn: "UsuarioEscolaId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Wizard",
                columns: table => new
                {
                    WizardId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UsuarioEscolaFk = table.Column<int>(nullable: true),
                    QTQuestoesTotal = table.Column<int>(nullable: false),
                    NMTitulo = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    DSWizart = table.Column<string>(type: "text", nullable: true),
                    DTCriacao = table.Column<DateTime>(type: "datetime", nullable: true),
                    DTAtualizacao = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wizard", x => x.WizardId);
                    table.ForeignKey(
                        name: "WizardUsuario___fk",
                        column: x => x.UsuarioEscolaFk,
                        principalTable: "UsuarioEscola",
                        principalColumn: "UsuarioEscolaId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Questao",
                columns: table => new
                {
                    QuestaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NMQuestao = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    TXQuestao = table.Column<string>(type: "text", nullable: false),
                    DisciplinaFk = table.Column<int>(nullable: false),
                    UsuarioEscolaFk = table.Column<int>(nullable: false),
                    TipoQuestaoFk = table.Column<int>(nullable: false),
                    NivelDificuldadeQuestaoFk = table.Column<int>(nullable: false),
                    FLPermitirCompartilhar = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('N')"),
                    DTCriacao = table.Column<DateTime>(type: "datetime", nullable: false),
                    DTAtualizacao = table.Column<DateTime>(type: "datetime", nullable: true),
                    FLAtivo = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('A')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questao", x => x.QuestaoId);
                    table.ForeignKey(
                        name: "DisciplinaQuestao___fk",
                        column: x => x.DisciplinaFk,
                        principalTable: "Disciplina",
                        principalColumn: "DisciplinaId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "NivelDificuldadeQuestao___fk",
                        column: x => x.NivelDificuldadeQuestaoFk,
                        principalTable: "NivelDificuldadeQuestao",
                        principalColumn: "NivelDificuldadeQuestaoId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "TipoQuestao___fk",
                        column: x => x.TipoQuestaoFk,
                        principalTable: "TipoQuestao",
                        principalColumn: "TipoQuestaoId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "UsuarioEscolaQuestao___fk",
                        column: x => x.UsuarioEscolaFk,
                        principalTable: "UsuarioEscola",
                        principalColumn: "UsuarioEscolaId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Turma",
                columns: table => new
                {
                    TurmaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DisciplinaFk = table.Column<int>(nullable: false),
                    ProfessorFk = table.Column<int>(nullable: false),
                    TPClicloAvaliacao = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    QTClicloAvaliacao = table.Column<short>(nullable: false),
                    Ano = table.Column<short>(nullable: false),
                    Nome = table.Column<string>(unicode: false, maxLength: 75, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Turma", x => x.TurmaId);
                    table.ForeignKey(
                        name: "FK_Turma_Disciplina",
                        column: x => x.DisciplinaFk,
                        principalTable: "Disciplina",
                        principalColumn: "DisciplinaId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Turma_UsuarioEscola",
                        column: x => x.ProfessorFk,
                        principalTable: "UsuarioEscola",
                        principalColumn: "UsuarioEscolaId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DisciplinaWizart",
                columns: table => new
                {
                    DisciplinaWizartId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DisciplinaFk = table.Column<int>(nullable: false),
                    WizartFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DisciplinaWizart", x => x.DisciplinaWizartId);
                    table.ForeignKey(
                        name: "DisciplinaWizartDisciplina___fk",
                        column: x => x.DisciplinaFk,
                        principalTable: "Disciplina",
                        principalColumn: "DisciplinaId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "DisciplinaWizartWizart___fk",
                        column: x => x.WizartFk,
                        principalTable: "Wizard",
                        principalColumn: "WizardId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TipoQuestaoWizart",
                columns: table => new
                {
                    TipoQuestaoQuestionarioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    WizartFk = table.Column<int>(nullable: false),
                    TipoQuestaoFk = table.Column<int>(nullable: false),
                    QTQuestoes = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoQuestaoWizart", x => x.TipoQuestaoQuestionarioId);
                    table.ForeignKey(
                        name: "TipoQuestaoWizartTPQuestao___fk",
                        column: x => x.TipoQuestaoFk,
                        principalTable: "TipoQuestao",
                        principalColumn: "TipoQuestaoId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "TipoQuestaoWizart_Wizard_WizardId_fk",
                        column: x => x.WizartFk,
                        principalTable: "Wizard",
                        principalColumn: "WizardId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Alternativa",
                columns: table => new
                {
                    AlternativaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestaoFk = table.Column<int>(nullable: false),
                    TXAlternativa = table.Column<string>(type: "text", nullable: false),
                    NROrdemAlternativa = table.Column<int>(nullable: false),
                    FLCorreta = table.Column<string>(unicode: false, maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alternativa", x => x.AlternativaId);
                    table.ForeignKey(
                        name: "QuestaoAlternativa___fk",
                        column: x => x.QuestaoFk,
                        principalTable: "Questao",
                        principalColumn: "QuestaoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuestaoCompartilhada",
                columns: table => new
                {
                    QuestaoCompartilhadaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuetaoFk = table.Column<int>(nullable: false),
                    UsuarioCompartilhadoFk = table.Column<int>(nullable: false),
                    DTCriacao = table.Column<DateTime>(type: "datetime", nullable: false),
                    DTAtualizacao = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestaoCompartilhada", x => x.QuestaoCompartilhadaId);
                    table.ForeignKey(
                        name: "QuestaoCompartilhada_Questao_QuestaoId_fk",
                        column: x => x.QuetaoFk,
                        principalTable: "Questao",
                        principalColumn: "QuestaoId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "QuestaoCompartilhadaUsuario___fk",
                        column: x => x.UsuarioCompartilhadoFk,
                        principalTable: "UsuarioEscola",
                        principalColumn: "UsuarioEscolaId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuestionarioGerado",
                columns: table => new
                {
                    QuestionarioGeradoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestaoFk = table.Column<int>(nullable: false),
                    QuestionarioFk = table.Column<int>(nullable: false),
                    DTCriacao = table.Column<DateTime>(type: "datetime", nullable: false),
                    DTAtualizacao = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionarioGerado", x => x.QuestionarioGeradoId);
                    table.ForeignKey(
                        name: "QuestaoQuestionario_Questao_QuestaoId_fk",
                        column: x => x.QuestaoFk,
                        principalTable: "Questao",
                        principalColumn: "QuestaoId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "QuestaoQuestionario_Questionario_QuestionarioId_fk",
                        column: x => x.QuestionarioFk,
                        principalTable: "Questionario",
                        principalColumn: "QuestionarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Resposta",
                columns: table => new
                {
                    RespostaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestaoFk = table.Column<int>(nullable: false),
                    TXResposta = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resposta", x => x.RespostaId);
                    table.ForeignKey(
                        name: "QuestaoResposta___fk",
                        column: x => x.QuestaoFk,
                        principalTable: "Questao",
                        principalColumn: "QuestaoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TurmaAluno",
                columns: table => new
                {
                    TurmaAlunoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TurmaFk = table.Column<int>(nullable: false),
                    AlunoFk = table.Column<int>(nullable: false),
                    NRClicloAvaliacao = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TurmaAluno", x => x.TurmaAlunoId);
                    table.ForeignKey(
                        name: "FK_TurmaAluno_UsuarioEscola",
                        column: x => x.AlunoFk,
                        principalTable: "UsuarioEscola",
                        principalColumn: "UsuarioEscolaId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TurmaAluno_Turma",
                        column: x => x.TurmaFk,
                        principalTable: "Turma",
                        principalColumn: "TurmaId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NivelDificuldadeWizart",
                columns: table => new
                {
                    NivelDificuldadeQuestionarioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NivelDificuldadeQuestaoFk = table.Column<int>(nullable: false),
                    DisciplinaWizartFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NivelDificuldadeWizart", x => x.NivelDificuldadeQuestionarioId);
                    table.ForeignKey(
                        name: "NivelDificuldadeWizartDisciplina___fk",
                        column: x => x.DisciplinaWizartFk,
                        principalTable: "DisciplinaWizart",
                        principalColumn: "DisciplinaWizartId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "NivelDificuldadeWizartNivel___fk",
                        column: x => x.NivelDificuldadeQuestaoFk,
                        principalTable: "NivelDificuldadeQuestao",
                        principalColumn: "NivelDificuldadeQuestaoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AlunoAleatorio",
                columns: table => new
                {
                    AlunoAleatorioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TurmaAlunoFk = table.Column<int>(nullable: false),
                    FLBloqueioSorteio = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    NRRodadaAtual = table.Column<short>(nullable: true),
                    DTUltimaGeracao = table.Column<DateTime>(type: "datetime", nullable: true),
                    Presente = table.Column<string>(unicode: false, maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlunoAleatorio", x => x.AlunoAleatorioId);
                    table.ForeignKey(
                        name: "FK_AlunoAleatorio_TurmaAluno",
                        column: x => x.TurmaAlunoFk,
                        principalTable: "TurmaAluno",
                        principalColumn: "TurmaAlunoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ParticipaoAluno",
                columns: table => new
                {
                    ParticipacaoAlunoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AlunoAleatorioFk = table.Column<int>(nullable: false),
                    DTSorteio = table.Column<DateTime>(type: "datetime", nullable: false),
                    NRRodada = table.Column<short>(nullable: true),
                    VLNotaParticipacao = table.Column<decimal>(type: "numeric(13, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParticipaoAluno", x => x.ParticipacaoAlunoId);
                    table.ForeignKey(
                        name: "FK_ParticipaoAluno_AlunoAleatorio",
                        column: x => x.AlunoAleatorioFk,
                        principalTable: "AlunoAleatorio",
                        principalColumn: "AlunoAleatorioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Alternativa_QuestaoFk",
                table: "Alternativa",
                column: "QuestaoFk");

            migrationBuilder.CreateIndex(
                name: "IX_AlunoAleatorio_TurmaAlunoFk",
                table: "AlunoAleatorio",
                column: "TurmaAlunoFk");

            migrationBuilder.CreateIndex(
                name: "IX_Curso_NivelCursoFk",
                table: "Curso",
                column: "NivelCursoFk");

            migrationBuilder.CreateIndex(
                name: "IX_Disciplina_CursoFk",
                table: "Disciplina",
                column: "CursoFk");

            migrationBuilder.CreateIndex(
                name: "IX_DisciplinaWizart_DisciplinaFk",
                table: "DisciplinaWizart",
                column: "DisciplinaFk");

            migrationBuilder.CreateIndex(
                name: "IX_DisciplinaWizart_WizartFk",
                table: "DisciplinaWizart",
                column: "WizartFk");

            migrationBuilder.CreateIndex(
                name: "IX_NivelDificuldadeWizart_DisciplinaWizartFk",
                table: "NivelDificuldadeWizart",
                column: "DisciplinaWizartFk");

            migrationBuilder.CreateIndex(
                name: "IX_NivelDificuldadeWizart_NivelDificuldadeQuestaoFk",
                table: "NivelDificuldadeWizart",
                column: "NivelDificuldadeQuestaoFk");

            migrationBuilder.CreateIndex(
                name: "IX_ParticipaoAluno_AlunoAleatorioFk",
                table: "ParticipaoAluno",
                column: "AlunoAleatorioFk");

            migrationBuilder.CreateIndex(
                name: "IX_Questao_DisciplinaFk",
                table: "Questao",
                column: "DisciplinaFk");

            migrationBuilder.CreateIndex(
                name: "IX_Questao_NivelDificuldadeQuestaoFk",
                table: "Questao",
                column: "NivelDificuldadeQuestaoFk");

            migrationBuilder.CreateIndex(
                name: "IX_Questao_TipoQuestaoFk",
                table: "Questao",
                column: "TipoQuestaoFk");

            migrationBuilder.CreateIndex(
                name: "IX_Questao_UsuarioEscolaFk",
                table: "Questao",
                column: "UsuarioEscolaFk");

            migrationBuilder.CreateIndex(
                name: "IX_QuestaoCompartilhada_QuetaoFk",
                table: "QuestaoCompartilhada",
                column: "QuetaoFk");

            migrationBuilder.CreateIndex(
                name: "IX_QuestaoCompartilhada_UsuarioCompartilhadoFk",
                table: "QuestaoCompartilhada",
                column: "UsuarioCompartilhadoFk");

            migrationBuilder.CreateIndex(
                name: "IX_Questionario_UsuarioEscolaFk",
                table: "Questionario",
                column: "UsuarioEscolaFk");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionarioGerado_QuestaoFk",
                table: "QuestionarioGerado",
                column: "QuestaoFk");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionarioGerado_QuestionarioFk",
                table: "QuestionarioGerado",
                column: "QuestionarioFk");

            migrationBuilder.CreateIndex(
                name: "IX_Resposta_QuestaoFk",
                table: "Resposta",
                column: "QuestaoFk");

            migrationBuilder.CreateIndex(
                name: "IX_TipoQuestaoWizart_TipoQuestaoFk",
                table: "TipoQuestaoWizart",
                column: "TipoQuestaoFk");

            migrationBuilder.CreateIndex(
                name: "IX_TipoQuestaoWizart_WizartFk",
                table: "TipoQuestaoWizart",
                column: "WizartFk");

            migrationBuilder.CreateIndex(
                name: "IX_Turma_DisciplinaFk",
                table: "Turma",
                column: "DisciplinaFk");

            migrationBuilder.CreateIndex(
                name: "IX_Turma_ProfessorFk",
                table: "Turma",
                column: "ProfessorFk");

            migrationBuilder.CreateIndex(
                name: "IX_TurmaAluno_AlunoFk",
                table: "TurmaAluno",
                column: "AlunoFk");

            migrationBuilder.CreateIndex(
                name: "IX_TurmaAluno_TurmaFk",
                table: "TurmaAluno",
                column: "TurmaFk");

            migrationBuilder.CreateIndex(
                name: "IX_UsuarioEscola_PessoaFk",
                table: "UsuarioEscola",
                column: "PessoaFk");

            migrationBuilder.CreateIndex(
                name: "IX_Wizard_UsuarioEscolaFk",
                table: "Wizard",
                column: "UsuarioEscolaFk");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Alternativa");

            migrationBuilder.DropTable(
                name: "NivelDificuldadeWizart");

            migrationBuilder.DropTable(
                name: "ParticipaoAluno");

            migrationBuilder.DropTable(
                name: "QuestaoCompartilhada");

            migrationBuilder.DropTable(
                name: "QuestionarioGerado");

            migrationBuilder.DropTable(
                name: "Resposta");

            migrationBuilder.DropTable(
                name: "TipoQuestaoWizart");

            migrationBuilder.DropTable(
                name: "DisciplinaWizart");

            migrationBuilder.DropTable(
                name: "AlunoAleatorio");

            migrationBuilder.DropTable(
                name: "Questionario");

            migrationBuilder.DropTable(
                name: "Questao");

            migrationBuilder.DropTable(
                name: "Wizard");

            migrationBuilder.DropTable(
                name: "TurmaAluno");

            migrationBuilder.DropTable(
                name: "NivelDificuldadeQuestao");

            migrationBuilder.DropTable(
                name: "TipoQuestao");

            migrationBuilder.DropTable(
                name: "Turma");

            migrationBuilder.DropTable(
                name: "Disciplina");

            migrationBuilder.DropTable(
                name: "UsuarioEscola");

            migrationBuilder.DropTable(
                name: "Curso");

            migrationBuilder.DropTable(
                name: "Pessoa");

            migrationBuilder.DropTable(
                name: "NivelCurso");
        }
    }
}
